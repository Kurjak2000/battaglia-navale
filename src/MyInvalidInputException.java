
public class MyInvalidInputException extends Exception {
	
	private int positionX;
	private int positionY;

	public MyInvalidInputException(String message, int positionX, int positionY) {
		super(message);
		this.positionX = positionX;
		this.positionY = positionY;
	}
	
	public MyInvalidInputException(String message) {
		super(message);
	}

	public int getPositionX() {
		return positionX;
	}	
	
	public int getPositionY() {
		return positionY;
	}
}
