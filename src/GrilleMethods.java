
/**
 * 
 * @author HumanBooster
 *
 */
public class GrilleMethods {

	private int rows;
	private int cols;
	private boolean [] arrayListB3;
	private boolean [] arrayListB4;
	private boolean [] arrayListB5;
	
	public GrilleMethods(int rows, int cols) {
		// TODO Auto-generated constructor stub
		this.setRows(rows);
		this.setCols(cols);
		this.arrayListB3 = new boolean [rows * cols];
		this.arrayListB4 = new boolean [rows * cols];
		this.arrayListB5 = new boolean [rows * cols];
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}
	
	
	public boolean EstPositionnableOccupe(int x, int y, int dim, char orientation) throws MyInvalidInputException {
		exceptionDehorsGrille(x, y, dim, orientation); //le premier point tombe dans la grille
		boolean result = false;
		
		switch (dim) 
		{
			case 3:
				result = EstPositionnableOccupe(x, y, dim, orientation, arrayListB3);
			break;
				
			case 4:
				result = EstPositionnableOccupe(x, y, dim, orientation, arrayListB4);					
			break;
				
			case 5:
				result = EstPositionnableOccupe(x, y, dim, orientation, arrayListB5);					
			break;

			default:
			break;
		}
		
		return result;
	}
	
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param dim
	 * @param orientation
	 * @param array
	 * @return
	 */
	private boolean EstPositionnableOccupe(int x, int y, int dim, char orientation, boolean[] array) {
		boolean result = true;

		for (int i = 0; i < dim; i++) {
			if (orientation == 'H') {
				if (array[x + i + ((y) * this.cols)] == true) // il est occupé
				{
					result = false;
					break;
				}
			} 
			else // orientation Vertical
			{
				if (array[x + (((y) + i) * this.cols)] == true) // il est occupé
				{
					result = false;
					break;
				}
			}
		}

		return result;
	}
	
	
	public void occupePositionne (int x, int y, int dim, char orientation) throws MyInvalidInputException {
		exceptionDehorsGrille(x, y, dim, orientation); //le premier point tombe dans la grille	
		
		switch (dim) 
		{
			case 3:
				occupePositionne(x, y, dim, orientation, arrayListB3);
			break;
				
			case 4:
				occupePositionne(x, y, dim, orientation, arrayListB4);					
			break;
				
			case 5:
				occupePositionne(x, y, dim, orientation, arrayListB5);					
			break;

			default:
			break;
		}
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param dim
	 * @param orientation
	 * @param array
	 * @return
	 */
	private void occupePositionne (int x, int y, int dim, char orientation, boolean [] array) {

		for (int i = 0; i < dim; i++) {
			if (orientation == 'H') {
				array[x + i + ((y) * this.cols)] = true; // occupé
			} else // orientation Vertical
			{
				array[x + (((y) + i) * this.cols)] = true; // occupé
			}
		}
	}

	/**
	 * 
	 * @param x	: position sur colonnes
	 * @param y : position sur lignes
	 * @param dim : la longueur du bateau
	 * @param orientation : orientation vertical ou horizontal du bateau
	 * @throws MyInvalidInputException
	 */
	public void exceptionDehorsGrille(int x, int y, int dim, char orientation) throws MyInvalidInputException {
		 
		if (x > this.cols && y > this.rows)
			throw new MyInvalidInputException("Cette position est dehors de la grille: " + x + "," + y,	x, y);
		
		if ((orientation == 'H') && (x+dim > this.cols))
				throw new MyInvalidInputException("Cette position est dehors de la grille: " + x + "," + y + " dimension (" + dim + ")",	x, y);
		else if ((orientation == 'V') && (y+dim > this.rows))
			throw new MyInvalidInputException("Cette position est dehors de la grille: " + x + "," + y + " dimension (" + dim + ")",	x, y);
		else if ((orientation != 'V') && (orientation != 'H'))
			throw new MyInvalidInputException("Orientation invalide: " + x + "," + y,	x, y);

	}
}
