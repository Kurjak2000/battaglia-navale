public class Joueur {
	private String nom;
	private int score;
	private int [] coupsX;
	private int [] coupsY;
	private int nCoups;
	private int nCoupTire;
	



	public Joueur(String nom) {
		super();
		this.nom = nom;
		this.score = 0;
		this.nCoups = 10;
		this.nCoupTire = 0;
		this.coupsX = new int [] {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
		this.coupsY = new int [] {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	}
	
	public int getNCoupTire() {
		return nCoupTire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public int[] getCoupsX() {
		return coupsX;
	}

	public void setCoupsX(int[] coupsX) {
		this.coupsX = coupsX;
	}

	public int[] getCoupsY() {
		return coupsY;
	}

	public void setCoupsY(int[] coupsY) {
		this.coupsY = coupsY;
	}
	
	public void setTireCoup(int valCoupX, int valCoupY) throws MyInvalidInputException {
		exceptionDejaTire(valCoupX, valCoupY);
		this.coupsX[nCoupTire] = valCoupX;
		this.coupsY[nCoupTire] = valCoupY;
		nCoupTire++;
	}
	
	public int getNumCoups() {
		return nCoups;
	}
	
	public void exceptionDejaTire(int valX, int valY) throws MyInvalidInputException {
		for (int a = 0 ; a < this.nCoupTire; a++) {
			if (this.coupsX[a] == valX && this.coupsY[a] == valY )
				throw new MyInvalidInputException("Coup déjà tiré sur cette position : " + valX + "," + valY , valX , valY);
		}
	}

}
