import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;


/**
 * 
 */

/**
 * @author HumanBooster
 *
 */
public class Partie extends JFrame {
	
	public Partie(int rows, int cols) {
		super();
		// getContentPane().setLayout(new BorderLayout(20, 20));
		this.setResizable(false);

		JPanel pan_Principal = new JPanel();
		getContentPane().add(pan_Principal);
		GridBagLayout gbl_pan_Principal = new GridBagLayout();
		gbl_pan_Principal.columnWidths = new int[] { 0, 0 };
		gbl_pan_Principal.rowHeights = new int[] { 0 };
		gbl_pan_Principal.columnWeights = new double[] { 40.0, 20.0 }; // Double.MIN_VALUE
		gbl_pan_Principal.rowWeights = new double[] { 40.0 };
		pan_Principal.setLayout(gbl_pan_Principal);

		GridBagConstraints gbc_pan_grigle = new GridBagConstraints();
		gbc_pan_grigle.anchor = GridBagConstraints.NORTHWEST;
		// gbc_pan_grigle.insets = new Insets(0, 0, 5, 5);
		gbc_pan_grigle.gridx = 0;
		gbc_pan_grigle.gridy = 0;

		GrilleDesign myGrilleDesign = new GrilleDesign(rows, cols);
		pan_Principal.add(myGrilleDesign, gbc_pan_grigle);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int lGrille = 10;
		int hGrille = 10;
		int numEnemies = 3;

		GrilleMethods myGrilleMethods = new GrilleMethods(hGrille, lGrille);
		Joueur myJoueur = new Joueur("Massimo");
		Enemy[] flotteEnemies = new Enemy[numEnemies];
		
		JFrame moiJF = new Partie(myGrilleMethods.getRows(), myGrilleMethods.getCols());

		initGame(flotteEnemies, numEnemies, myGrilleMethods);
		testGame2(myJoueur, flotteEnemies, numEnemies, lGrille, hGrille);
	}


	private static void initGame(Enemy[] flotteEnemies, int numEnemies, GrilleMethods myGrilleMethods) {

		System.out.println(" ----- Initialisation ----");

		Random numRandom = new Random();
		int posRow = 0;
		int posCol = 0;

		int dimension = 0;
		int limitMinBoat = 3;
		int limitMaxBoat = 5 - limitMinBoat;

		char orientation = 'V';

		boolean isPositionne = false;
		int numEnemiesCree = 0;
		String nameEnemy = "";
		char moveBoatV = '+';
		char moveBoatH = '+';
		
		int countTry = 0;

		try {
			for (int c = 0; c < numEnemies; c++) { // creo tutti i nemici
				nameEnemy = "";
				orientation = 'V';
				countTry = 0;
				
				dimension = numRandom.nextInt(limitMaxBoat) + limitMinBoat;// Valori compresi tra 3 e 5
				posRow = numRandom.nextInt(myGrilleMethods.getRows());
				posCol = numRandom.nextInt(myGrilleMethods.getCols());
				
				
				// se la nave fuori esce dalla griglia, sottraggo alla posizione iniziale l'eccedenza
				posRow = (orientation == 'V') && (posRow + dimension > myGrilleMethods.getRows()) 
						? posRow - (posRow + dimension - myGrilleMethods.getRows())
						: posRow;
						
				posCol = (orientation == 'H') && (posCol + dimension > myGrilleMethods.getCols()) 
						? posCol - (posCol + dimension - myGrilleMethods.getCols())
						: posCol;
						
				//qui analizzo se nei successivi tentativi di posizionamento della nave devo provare a spostarla in avanti o
				// indietro, in base al fatto che la prima posizione sia piu' avanti o piu' indietro della meta' della griglia
				if (posRow > (myGrilleMethods.getRows()/2))
					moveBoatV = '-';
				
				if (posCol > (myGrilleMethods.getCols()/2))
					moveBoatH = '-';
				

				while (isPositionne == false && countTry < 6) {

					if (orientation == 'V')
					{
						if (moveBoatH == '+') //la nave e' orientata in verticale e cerco una posizione sull'asse orizzontale
							posCol += countTry;
						else
							posCol -= countTry;
					}
					else
					{
						if (moveBoatV == '+') //la nave e' orientata in orizzontale e cerco una posizione sull'asse verticale
							posRow += countTry;
						else
							posRow -= countTry;
					}
					
					if (numEnemiesCree == 0) // se è il primo lo posiziono senza fare i controlli
						isPositionne = true;
					else {
						if (countTry == 3) // provo un altro verso
							orientation = 'H';

						// controllo se rientra nella griglia
						isPositionne = myGrilleMethods.EstPositionnableOccupe(posCol, posRow, dimension, orientation);

						// controllo tra i nemici creati se non va in conflitto di posizione
						for (int k = 0; k < numEnemiesCree; k++) {
							isPositionne = flotteEnemies[k].isOccupyOfYou(posCol, posRow);
						}

					}
					
					if (isPositionne == true)
						myGrilleMethods.occupePositionne(posCol, posRow, dimension, orientation);

					countTry++;
				}

				nameEnemy = "Enemy-" + c; // creation
				flotteEnemies[c] = new Enemy(nameEnemy, 3, posCol, posRow, orientation);
				numEnemiesCree++;
				isPositionne = false;
			}

			System.out.println(" **** Fin Initialisation ****");
		} /*
		 * catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		catch (ArrayIndexOutOfBoundsException aiobe) {
			// TODO Auto-generated catch block
			System.out.println(" ATTENTION!!! l'indice est en dehors des limites du tableau ");
		} catch (MyInvalidInputException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void testGame2(Joueur myJoueur, Enemy[] flotteEnemies, int numEnemies, int lGrille, int hGrille) {
		boolean isHit = false;
		int nEnemiesMorts = 0;
		int valX = -1;
		int valY = -1;

		// Random coupRandom = new Random();
		Scanner scanMain = new Scanner(System.in);

		System.out.println(" ");
		System.out.println(" ----- Start Test 2 ----");
		System.out.println(" Nom de Joueur :");
		myJoueur.setNom(scanMain.nextLine());

		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ---> Tu peux tiré " + myJoueur.getNumCoups()
				+ " coups de feu");

		try {
			for (int i = 0; i < myJoueur.getNumCoups(); i++) {
				System.out.println(" ");
				System.out.println(" Ou tiré le coup de feu?");

				System.out.println(" pos X [0 - " + lGrille + "] :");
				valX = scanMain.nextInt();
				scanMain.nextLine();

				System.out.println(" pos Y [0 - " + hGrille + "] :");
				valY = scanMain.nextInt();
				scanMain.nextLine();

				if ((valX < 0 || valX > lGrille) && (valY < 0 || valY > hGrille))
					throw new MyInvalidInputException("Ce coup est hors limites", valX, valY);

				myJoueur.setTireCoup(valX, valY);

				for (int k = 0; k < numEnemies; k++) {
					isHit = flotteEnemies[k].isHit(valX, valY);

					if (isHit == true) {
						System.out.println("   bien Joue  ");
						System.out.println("   --- Ennemie '" + flotteEnemies[k].getName() + "'");
						System.out.println(" et il a été endommagé " + flotteEnemies[k].getNumDamage());
					} else
						System.out.println(myJoueur.getNom() + " a rate dans l'eau  ");

					if (flotteEnemies[k].isDead()) {
						System.out.println("   Mort : " + flotteEnemies[k].getName());
						nEnemiesMorts++;

						if (nEnemiesMorts == numEnemies) {
							System.out.println(" TU AS GAGNE!!!!");
							return;
						}
					}

					System.out.println(" ");
				}

			}
		} catch (ArrayIndexOutOfBoundsException | InputMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MyInvalidInputException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// System.err.println("Le joueur a essayé de tirer à nouveau sur la case "
			// + e.getPosition());
			System.out.println(e.getMessage());
		}

		System.out.println(" ----- End Test ----");
	}
}
