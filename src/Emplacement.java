
public class Emplacement {

	private int posCol;
	private int posRow;
	
	
	
	public Emplacement(int pCol, int pRow) {
		// TODO Auto-generated constructor stub
		
		posCol = pCol;
		posRow = pRow;
	}

	public int getPosCol() {
		return posCol;
	}

	public void setPosCol(int posCol) {
		this.posCol = posCol;
	}

	public int getPosRow() {
		return posRow;
	}

	public void setPosRow(int posRow) {
		this.posRow = posRow;
	}

}
