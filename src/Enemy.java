public class Enemy {
	private String name;
	private int lenBlocks;
	private int firstPositionRow;
	private int firstPositionCol;
	private char orientation;
	private int numDamage;
	private boolean isPositioned;

	public Enemy(String name, int lenBlocks, int firstPositionCol, int firstPositionRow, char orientation) {
		super();
		this.name = name;
		this.lenBlocks = lenBlocks;
		this.firstPositionRow = firstPositionRow;
		this.firstPositionCol = firstPositionCol;
		this.orientation = orientation;
		this.numDamage = 0;
		this.isPositioned = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumDamage() {
		return numDamage;
	}

	public void setNumDamage(int numDamage) {
		this.numDamage = numDamage;
	}

	public boolean isPositioned() {
		return isPositioned;
	}

	public void setPositioned(boolean isPositioned) {
		this.isPositioned = isPositioned;
	}

	public int getLenBlocks() {
		return lenBlocks;
	}

	public void setLenBlocks(int lenBlocks) {
		this.lenBlocks = lenBlocks;
	}
	
	public char getOrientation() {
		return orientation;
	}

	public void setOrientation(char orientation) {
		this.orientation = orientation;
	}

	public boolean isHit(int pointX, int pointY) {
		boolean result = false;

		if (firstPositionRow <= pointY && pointY <= (firstPositionRow + (orientation=='V' ? lenBlocks : 0)) &&
			firstPositionCol <= pointX && pointX <= (firstPositionCol + (orientation=='H' ? lenBlocks : 0))) {
			result = true;
			numDamage++;
		} else
			result = false;

		return result;
	}
	
	public boolean isOccupyOfYou(int pointX, int pointY) {
		boolean result = false;

		if (firstPositionRow <= pointY && pointY <= (firstPositionRow + (orientation=='V' ? lenBlocks : 0)) &&
			firstPositionCol <= pointX && pointX <= (firstPositionCol + (orientation=='H' ? lenBlocks : 0))) {
			result = true;
		} else
			result = false;

		return result;
	}
	
	public int getFirstPositionRow() {
		return firstPositionRow;
	}

	public void setFirstPositionRow(int firstPositionH) {
		this.firstPositionRow = firstPositionH;
	}

	public int getFirstPositionCol() {
		return firstPositionCol;
	}

	public void setFirstPositionCol(int firstPositionL) {
		this.firstPositionCol = firstPositionL;
	}


	public boolean isDead() {
		boolean result = false;

		if (numDamage == 3)
			result = true;
		else
			result = false;

		return result;
	}

}
