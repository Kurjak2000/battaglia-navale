import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class GrilleDesign extends JPanel {
	
	private class Cella extends JPanel {
		private boolean colorata;
		private Color bckColore;

		public Cella() {
			setOpaque(true);
			this.setSize(200, 200);
			bckColore = getBackground();
			setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		}

		public void switchColore() {
			setBackground(colorata ? bckColore : Color.GREEN);
			colorata = !colorata;
		}
	}

	private class Clicker extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent me) {
			coloraDecolora((Cella) me.getSource());
		}
	}

	private Clicker clicker;
	private Cella[] celle;
	private boolean[] colora;

	public GrilleDesign(int rows, int cols) {

		FlowLayout flowLayout = (FlowLayout) this.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);
		
		LayoutManager myLM = new GridLayout(rows, cols);
		setLayout(myLM);
		this.setSize(200*rows, 200*cols);

		
		celle = new Cella[rows * cols];
		
		colora = new boolean[rows * cols];
		clicker = new Clicker();

		for (int i = 0; i < rows * cols; i++) {
			Cella c = new Cella();
			c.addMouseListener(clicker);
			add(c);
		}
	}

	private void coloraDecolora(Cella c) {
		c.switchColore();
	}
}
